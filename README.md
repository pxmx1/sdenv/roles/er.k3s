Role Name
=========

Build a Kubernetes cluster using Ansible with k3s.
Angepasste Rolle aus https://github.com/k3s-io/k3s-ansible

Requirements
------------

-

Role Variables
--------------

Doku siehe k3s-ansible

Variable | Description | Default
-------- | ----------- | -------
`k3s_version`| |`v1.28.3+k3s2`
`systemd_dir`| |`/etc/systemd/system`
`master_ip`| |`{{ hostvars[groups['master'][0]]['ansible_host'] \| defaul(groups['master'][0]) }}`
`extra_server_args`| | 
`extra_agent_args`| | 
`k3s_server_location`| | `/var/lib/rancher/k3s`



Dependencies
------------

-

Example Playbook
----------------

Inventory:

    [awx]
    awx.dns.local

    [master]
    awx.dns.local

    [node]
    awx.dns.local

Playbook:

    - name: Deploy a Ubuntu Cloud Image Virtual Appliance AWX Server
      hosts: awx
      gather_facts: false

      roles:
        - role: lra.vmware_ubuntu_cloud_image
          vars:
            annotation: '{{inventory_hostname}} - AWX Server - automated installation'
            disk:
              - size_gb: 30
                datastore: san-nfs
                scsi_controller: 0
                unit_number: 0
            hardware:
              num_cpus: 8
              memory_mb: 12288

        - role: lra.ubuntu
        - role: lra.ipa_client

        - role: lra.k3s


License
-------

BSD

Author Information
------------------

Engelbert Roidl
